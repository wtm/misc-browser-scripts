(function () {
  const LS_PRESSED_LOGIN = "__mw_pressed_login_button";
  const LS_SHOULD_GO_TO_HOLDS = "__mw_should_go_to_holds";
  function start() {
    let login_link = document.querySelector(".loginLink");
    if (login_link && login_link.innerText.indexOf("Log In") >= 0) {
      if (localStorage.getItem(LS_PRESSED_LOGIN) === "true") {
        // Login failed. Don't try again.
        return;
      }
      login_link.click();
      setTimeout(() => {
        let form = document.querySelector("#loginPageForm");
        let submit = form.querySelector("#submit_0");
        localStorage.setItem(LS_PRESSED_LOGIN, "true");
        submit.click();
      }, 10);
    } else if (localStorage.getItem(LS_PRESSED_LOGIN)) {
      localStorage.removeItem(LS_PRESSED_LOGIN);
      localStorage.setItem(LS_SHOULD_GO_TO_HOLDS, "true");
      window.location = "https://trib.ent.sirsidynix.net.uk/client/en_GB/wcc/search/account?";
    } else if (localStorage.getItem(LS_SHOULD_GO_TO_HOLDS)) {
      localStorage.removeItem(LS_SHOULD_GO_TO_HOLDS);
      document.querySelector("a[href=\"#holdsTab\"]").click();
    }
  }

  if (document.readyState == "loading") {
    document.addEventListener("DOMContentLoaded", start);
  } else {
    start();
  }
})();
